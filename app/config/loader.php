<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'SimpleBlog\Models' => APP_PATH . '/common/models/',
    'SimpleBlog'        => APP_PATH . '/common/library/',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'SimpleBlog\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'SimpleBlog\Modules\Admin\Module'    => APP_PATH . '/modules/admin/Module.php',
    'SimpleBlog\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();
