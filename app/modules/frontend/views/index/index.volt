{% extends "index.volt" %}

{% block content %}
<div class="page-header">
    <h1>Congratulations!</h1>
</div>

<p>You're now flying with Phalcon. Great things are about to happen!</p>

<p>This page is located at <code>forntend/views/index/index.volt</code></p>
{% endblock %}