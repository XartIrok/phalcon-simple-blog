<?php

namespace SimpleBlog\Modules\Admin\Controllers;

use SimpleBlog\Models\Users;

class SessionController extends ControllerBase
{
    public function indexAction()
    {
        if ($this->request->isPost()) {
            $userCheckExist = Users::findFirst(array(
                'login = :login: AND is_admin = 1',
                'bind' => array('login' => $this->request->getPost('login'))
            ));

            if ($userCheckExist) {
                if ($this->security->checkHash($this->request->getPost('pass'), $userCheckExist->pass)) {
                    $this->setAuth($userCheckExist);
                    $this->flash->success('Witaj administratorze');
                    return $this->response->redirect('main');
                }
                else {
                    $this->flash->error('wprowadzone nie poprawne dane do formularza');
                }
            }
            else {
                $this->flash->warning('Użytkownmik nie istnieje');
            }
        }
    }

    private function setAuth(Users $user)
    {
        $this->session->set('dashboard', array(
            'user' => $user
        ));
    }

}

