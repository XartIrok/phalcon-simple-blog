<?php
namespace SimpleBlog\Modules\Admin\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->url->setBaseUri('/admin/');
        if (!$this->checkSession() && $this->router->getControllerName() != 'session') {
            $this->flash->warning('Nie jesteś zalogowany');
            $this->response->redirect('session');
        }
    }

    public function checkSession()
    {
        return $this->session->has('dashboard') ? true : false;
    }
}
